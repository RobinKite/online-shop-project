# Online-shop-project

This project is created with the help from tutorials of [MaxGraph youtube channel](https://www.youtube.com/watch?v=sMFhxZjCaI4&list=PLCMvV-acWe2DOE8ZBL2M5g2O70noa6A12). Also his [github](https://github.com/maxdenaro) and [project on figma](https://www.figma.com/file/28GCW0PvfdyEAU2T39rjNc/ui-components?node-id=72%3A449&t=juuuVLaHL3WTCLh3-0).

Link to the project on gitlab pages: [here](https://robinkite.gitlab.io/online-shop-project).
