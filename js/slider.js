const swiper = new Swiper('.slider-block', {
	slidesPerView: 1,
});

const maxItems = 5;

const sliderNavItems = document.querySelectorAll('.slider-nav__item');
const sliderNav = document.querySelector('.slider-nav');

const showMore = () => {
	const length = sliderNav.children.length;

	if (length > maxItems) {
		const btn = document.createElement('button');
		btn.classList.add('show-more');
		btn.textContent = `${length - maxItems} more`;

		btn.addEventListener('click', (e) => {
			new GraphModal().open('one');
		});

		sliderNavItems.forEach((el, index) => {
			el.style.display = index + 1 > maxItems ? 'none' : null;
		});
		sliderNav.insertAdjacentElement('beforeend', btn);
	}
};

showMore();

sliderNavItems.forEach((el, index) => {
	el.dataset.index = index;

	el.addEventListener('click', (e) => {
		const index = parseInt(e.currentTarget.dataset.index);

		swiper.slideTo(index);
	});
});
