const showMoreBtn = document.querySelector('.btn-center__show-more');
const productsLength = document.querySelectorAll('.products-grid__item').length;
let items = 3;

showMoreBtn.addEventListener('click', (e) => {
	items += 3;
	const array = Array.from(document.querySelector('.products-grid').children);
	const visibleItems = array.slice(0, items);

	visibleItems.forEach((el) =>
		el.classList.add('products-grid__item--visible')
	);

	if (visibleItems.length === productsLength) {
		showMoreBtn.style.display = 'none';
	}
});
