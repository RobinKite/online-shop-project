document.addEventListener('DOMContentLoaded', () => {
	const productsBtn = document.querySelectorAll('.product__btn');
	const cartProductsList = document.querySelector('.cart-content__list');
	const cart = document.querySelector('.cart');
	const cartQuantity = document.querySelector('.cart__quantity');
	const fullPrice = document.querySelector('.fullprice');
	const orderModalOpenProd = document.querySelector('.order-modal__btn');
	const orderModalList = document.querySelector('.order-modal__list');
	const productArray = [];
	let price = 0;
	let randomId = 0;

	// const randomId = () => {
	// 	return (
	// 		Math.random().toString(36).substring(2, 15) +
	// 		Math.random().toString(36).substring(2, 15)
	// 	);
	// };

	const priceWithoutSpaces = (str) => {
		return str.replace(/\s/g, '');
	};

	const normalPrice = (str) => {
		return String(str).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
	};

	const plusFullPrice = (currentPrice) => {
		return (price += currentPrice);
	};

	const minusFullPrice = (currentPrice) => {
		return (price -= currentPrice);
	};

	const printFullPrice = () => {
		fullPrice.textContent = `${normalPrice(price)} ₴`;
	};

	const printQuantity = () => {
		const length = cartProductsList.querySelectorAll(
			'.cart-content__item'
		).length;
		cartQuantity.textContent = length;
		length > 0 ? cart.classList.add('active') : cart.classList.remove('active');
	};

	const generateCartProduct = (img, title, price, id) => {
		return `<li class="cart-content__item">
		<article class="cart-content__product cart-product" data-id="${id}">
			<img src="${img}" alt="Macbook" class="cart-product__img">
			<div class="cart-product__text">
				<h3 class="cart-product__title">${title}</h3>
				<span class="cart-product__price">${normalPrice(price)} ₴</span>
			</div>
			<button class="cart-product__delete" aria-label="Delete product"></button>
		</article>
	</li>`;
	};

	const deleteProducts = (productParent) => {
		const id = productParent.querySelector('.cart-product').dataset.id;
		document
			.querySelector(`.product[data-id="${id}"]`)
			.querySelector('.product__btn').disabled = false;
		const currentPrice = parseInt(
			priceWithoutSpaces(
				productParent.querySelector('.cart-product__price').textContent
			)
		);
		minusFullPrice(currentPrice);
		printFullPrice();
		productParent.remove();
		printQuantity();

		updateStorage();
	};

	productsBtn.forEach((el) => {
		el.closest('.product').dataset.id = randomId++;
		el.addEventListener('click', (e) => {
			const self = e.currentTarget;
			const parent = self.closest('.product');
			const id = parent.dataset.id;
			const img = parent
				.querySelector('.image-switch__img img')
				.getAttribute('src');
			const title = parent.querySelector('.product__title').textContent;
			const priceString = parent.querySelector(
				'.product-price__current'
			).textContent;
			const priceNumber = parseInt(priceWithoutSpaces(priceString));

			plusFullPrice(priceNumber);
			printFullPrice();
			cartProductsList
				.querySelector('.simplebar-content')
				.insertAdjacentHTML(
					'afterbegin',
					generateCartProduct(img, title, priceNumber, id)
				);
			printQuantity();

			updateStorage();

			self.disabled = true;
		});
	});

	cartProductsList.addEventListener('click', (e) => {
		if (e.target.classList.contains('cart-product__delete')) {
			deleteProducts(e.target.closest('.cart-content__item'));
		}
	});

	let flag = 0;
	orderModalOpenProd.addEventListener('click', (e) => {
		if (flag === 0) {
			orderModalList.style.display = 'block';
			orderModalOpenProd.classList.add('open');
			flag = 1;
		} else {
			orderModalList.style.display = 'none';
			orderModalOpenProd.classList.remove('open');
			flag = 0;
		}
	});

	const generateModalProduct = (img, title, price, id) => {
		return `<li class="order-modal__item">
		<article class="order-modal__product order-product" data-id="${id}">
			<img src="${img}" alt="Macbook" class="order-product__img">
			<div class="order-product__text">
				<h3 class="order-product__title">${title}</h3>
				<span class="order-product__price">${normalPrice(price)}</span>
			</div>
			<button class="order-product__delete">Remove</button>
		</article>
	</li>`;
	};

	const modal = new GraphModal({
		isOpen: (modal) => {
			// console.log('opened');
			const arr = cartProductsList.querySelectorAll('.cart-content__item');
			const fullprice = fullPrice.textContent;
			const length = arr.length;

			orderModalList.innerHTML = '';
			productArray.splice(0, productArray.length);

			document.querySelector(
				'.order-modal__quantity span'
			).textContent = `${length} pcs`;
			document.querySelector(
				'.order-modal__summ span'
			).textContent = `${fullprice}`;

			arr.forEach((item) => {
				const id = item.querySelector('.cart-product').dataset.id;
				const img = item
					.querySelector('.cart-product__img')
					.getAttribute('src');
				const title = item.querySelector('.cart-product__title').textContent;
				const price = priceWithoutSpaces(
					item.querySelector('.cart-product__price').textContent
				);

				orderModalList.insertAdjacentHTML(
					'afterbegin',
					generateModalProduct(img, title, price, id)
				);

				const obj = {};
				obj.id = id;
				obj.title = title;
				obj.price = price;
				productArray.push(obj);
			});
			console.log(productArray);
		},
		isClosed: () => {
			// console.log('closed');
		},
	});

	// document
	// 	.querySelector('.order')
	// 	.addEventListener('submit', (e) => {
	// 		e.preventDefault();
	// 		const self = e.currentTarget;
	// 		const formData = new FormData(self);
	// 		const name = self.querySelector('[name="Имя"]').value;
	// 		const tel = self.querySelector(
	// 			'[name="Телефон"]'
	// 		).value;
	// 		const mail = self.querySelector(
	// 			'[name="Почта"]'
	// 		).value;

	// 		// let xhr = new XMLHttpRequest();

	// 		formData.append('Имя: ', name);
	// 		formData.append('Телефон: ', tel);
	// 		formData.append('Почта: ', mail);
	// 		formData.append(
	// 			'Товары: ',
	// 			JSON.stringify(productArray)
	// 		);

	// 		// xhr.onreadystatechange = function () {
	// 		// 	if (xhr.readyState === 4) {
	// 		// 		if (xhr.stats === 200) {
	// 		// 			console.log('Отправлено');
	// 		// 		}
	// 		// 	}
	// 		// };
	// 		// xhr.open('POST', 'mail.php', true);
	// 		// xhr.send(FormData);

	// 		// self.reset();
	// 	});

	const countSumm = () => {
		const items = document.querySelectorAll('.cart-content__item');
		items.forEach((el) => {
			price += parseInt(
				priceWithoutSpaces(el.querySelector('.cart-product__price').textContent)
			);
		});
	};

	const initialState = () => {
		const products = localStorage.getItem('products');
		if (products) {
			const parent = cartProductsList.querySelector('.simplebar-content');
			parent.innerHTML = products;
			printQuantity();
			countSumm();
			printFullPrice();

			const items = document.querySelectorAll('.cart-content__product');
			items.forEach((el) => {
				const id = el.dataset.id;
				document
					.querySelector(`.product[data-id="${id}"]`)
					.querySelector('.product__btn').disabled = true;
			});
		}
	};

	initialState();

	const updateStorage = () => {
		const parent = cartProductsList.querySelector('.simplebar-content');
		const html = parent.innerHTML.trim();
		if (html.length) {
			localStorage.setItem('products', html);
		} else {
			localStorage.removeItem('products');
		}
	};

	document.querySelector('.modal').addEventListener('click', (e) => {
		if (e.target.classList.contains('order-product__delete')) {
			const parent = e.target.closest('.order-modal__product');
			const id = parent.dataset.id;
			const cartProduct = document
				.querySelector(`.cart-content__product[data-id="${id}"]`)
				.closest('.cart-content__item');
			deleteProducts(cartProduct);
			parent.remove();
		}
	});
});
