const fixedBlock = document.querySelector('.filters-price');
const filters = document.querySelector('.filters');
const container = document.querySelector('.container');
const gutter = parseInt(
	getComputedStyle(document.documentElement).getPropertyValue('--gutter')
);
const filtersOffsetTop = filters.offsetTop;
let filtersWidth = filters.offsetWidth;
let offsetLeft = container.offsetLeft + gutter;

const fixedScrollBlock = () => {
	let scrollDistance = window.scrollY;

	if (
		scrollDistance > filtersOffsetTop - gutter * 4 &&
		scrollDistance <= filters.offsetHeight + filtersOffsetTop
	) {
		offsetLeft = container.offsetLeft + gutter;
		filtersWidth = filters.offsetWidth;
		fixedBlock.style.left = `${offsetLeft}px`;
		fixedBlock.style.width = `${filtersWidth}px`;
		fixedBlock.classList.add('fixed');
		fixedBlock.classList.remove('absolute');
	} else {
		fixedBlock.style.left = `0px`;
		fixedBlock.style.width = null;

		fixedBlock.classList.remove('fixed');
	}

	if (
		scrollDistance >=
		filtersOffsetTop -
			gutter * 4 +
			filters.offsetHeight -
			fixedBlock.offsetHeight
	) {
		fixedBlock.style.left = `0px`;
		fixedBlock.style.width = null;
		fixedBlock.classList.add('absolute');
		fixedBlock.classList.remove('fixed');
	}
};

fixedScrollBlock();
window.addEventListener('scroll', fixedScrollBlock);
