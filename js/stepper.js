const stepper = document.querySelector('.stepper');
const stepperInput = document.querySelector('.stepper__input');
const stepperBtnUp = document.querySelector('.stepper__btn--up');
const stepperBtnDown = document.querySelector('.stepper__btn--down');
const inputMaxNumber = Math.pow(10, stepperInput.maxLength) - 1;

const regex = /^-?[0-9]+$/;

const isNotApple = () => {
	return !/iPhone|iPad|iPod/i.test(navigator.userAgent);
};

const disableBtns = () => {
	stepperBtnDown.classList[count == 1 ? 'add' : 'remove'](
		'stepper__btn--disabled'
	);
	stepperBtnUp.classList[count == inputMaxNumber ? 'add' : 'remove'](
		'stepper__btn--disabled'
	);
};

const exAmount = isNotApple ? 1 : 2;

let count = parseInt(stepperInput.value);

stepperInput.addEventListener('keyup', (e) => {
	let self = e.currentTarget;

	if (self.value === '0') self.value = 1;

	self.style.width = `${self.value.length + exAmount}ex`;

	count = parseInt(stepperInput.value);

	disableBtns();
});

stepperInput.addEventListener('keypress', (e) => {
	const code = e.which ? e.which : e.keyCode;
	if (code > 31 && (code < 48 || code > 57)) {
		e.preventDefault();
	}
});

stepperInput.addEventListener('change', (e) => {
	let self = e.currentTarget;

	if (!self.value || !self.value.match(regex)) self.value = 1;

	self.style.width = `${self.value.length + exAmount}ex`;

	count = parseInt(stepperInput.value);

	disableBtns();
});

stepperBtnUp.addEventListener('click', (e) => {
	e.preventDefault();

	count++;

	disableBtns();

	stepperInput.value = count;

	stepperInput.style.width = `${stepperInput.value.length + exAmount}ex`;
});

stepperBtnDown.addEventListener('click', (e) => {
	e.preventDefault();

	count--;

	disableBtns();

	stepperInput.value = count;

	stepperInput.style.width = `${stepperInput.value.length + exAmount}ex`;
});
