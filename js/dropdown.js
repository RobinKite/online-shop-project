document.addEventListener('DOMContentLoaded', () => {
	const menuBtns = document.querySelectorAll('.menu__btn');
	const drops = document.querySelectorAll('.dropdown');

	menuBtns.forEach((el) => {
		el.addEventListener('click', (e) => {
			const current = e.currentTarget;
			const drop = current.closest('.menu__item').querySelector('.dropdown');

			menuBtns.forEach((el) => {
				if (el !== current) el.classList.remove('menu__btn--active');
			});
			drops.forEach((el) => {
				if (el !== drop) el.classList.remove('dropdown--active');
			});

			current.classList.add('menu__btn--active');
			drop.classList.toggle('dropdown--active');
		});
	});

	document.addEventListener('click', (e) => {
		if (!e.target.closest('.menu__list')) {
			menuBtns.forEach((el) => {
				el.classList.remove('menu__btn--active');
			});
			drops.forEach((el) => {
				el.classList.remove('dropdown--active');
			});
		}
	});
});
