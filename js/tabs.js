document.addEventListener('DOMContentLoaded', () => {
	const tabs = document.querySelector('.tabs');
	const tabsBtn = document.querySelectorAll('.tabs__btn');
	const tabsContent = document.querySelectorAll('.tabs__content');

	if (tabs) {
		tabs.addEventListener('click', (e) => {
			if (e.target.classList.contains('tabs__btn')) {
				const tabsPath = e.target.dataset.tabsPath;
				tabsBtn.forEach((el) => {
					el.classList.remove('tabs__btn--active');
				});
				document
					.querySelector(`[data-tabs-path="${tabsPath}"]`)
					.classList.add('tabs__btn--active');
				tabsHandler(tabsPath);
			}

			if (e.target.closest('.tabs__arrow--prev')) {
				const activeBtn = document.querySelector('.tabs__btn--active');
				const activeParent = activeBtn.closest('.tabs__item');
				const prevParent = activeParent.previousElementSibling;
				if (prevParent) {
					const prevActive = prevParent.querySelector('.tabs__btn');
					const path = prevActive.dataset.tabsPath;

					activeBtn.classList.remove('tabs__btn--active');
					prevActive.classList.add('tabs__btn--active');
					tabsHandler(path);
				}
			}
			if (e.target.closest('.tabs__arrow--next')) {
				const activeBtn = document.querySelector('.tabs__btn--active');
				const activeParent = activeBtn.closest('.tabs__item');
				const nextParent = activeParent.nextElementSibling;
				if (nextParent) {
					const nextActive = nextParent.querySelector('.tabs__btn');
					const path = nextActive.dataset.tabsPath;
					activeBtn.classList.remove('tabs__btn--active');
					nextActive.classList.add('tabs__btn--active');
					tabsHandler(path);
				}
			}
		});
	}

	const tabsHandler = (path) => {
		tabsContent.forEach((el) => {
			el.classList.remove('tabs__content--active');
		});
		document
			.querySelector(`[data-tabs-target="${path}"]`)
			.classList.add('tabs__content--active');
	};
});
