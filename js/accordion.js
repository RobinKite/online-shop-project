document.addEventListener('DOMContentLoaded', (e) => {
	const accordions = document.querySelectorAll('.accordion__item');

	const contentPadding = 20;

	accordions.forEach((el) => {
		el.addEventListener('click', (e) => {
			const self = e.currentTarget;
			const control = self.querySelector('.accordion__control');
			const content = self.querySelector('.accordion__content');
			const condition = self.classList.contains('open');

			self.classList.toggle('open');

			control.setAttribute('aria-expanded', condition);
			content.setAttribute('aria-hidden', !condition);

			content.style.maxHeight = condition
				? null
				: `${content.scrollHeight + contentPadding * 4}px`;
		});
	});
});
